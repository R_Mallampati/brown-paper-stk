stk.v.11.0
WrittenBy    STK_v11.6.0

BEGIN Radar

    Name		 Radar1
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_Object">
    <SCOPE Class = "CommRadarObject">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_Object&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar Object&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar Object&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar Object&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Model">
            <VAR name = "Bistatic_Transmitter">
                <SCOPE Class = "Radar System">
                    <VAR name = "Version">
                        <STRING>&quot;1.0.0 a&quot;</STRING>
                    </VAR>
                    <VAR name = "STKVersion">
                        <INT>1160</INT>
                    </VAR>
                    <VAR name = "ComponentName">
                        <STRING>&quot;Bistatic_Transmitter&quot;</STRING>
                    </VAR>
                    <VAR name = "Description">
                        <STRING>&quot;Bistatic Transmitter&quot;</STRING>
                    </VAR>
                    <VAR name = "Type">
                        <STRING>&quot;Bistatic Transmitter&quot;</STRING>
                    </VAR>
                    <VAR name = "UserComment">
                        <STRING>&quot;Bistatic Transmitter&quot;</STRING>
                    </VAR>
                    <VAR name = "ReadOnly">
                        <BOOL>false</BOOL>
                    </VAR>
                    <VAR name = "Clonable">
                        <BOOL>true</BOOL>
                    </VAR>
                    <VAR name = "Category">
                        <STRING>&quot;@Top&quot;</STRING>
                    </VAR>
                    <VAR name = "AntennaControl">
                        <SCOPE>
                            <VAR name = "AntennaReferenceType">
                                <STRING>&quot;Embed&quot;</STRING>
                            </VAR>
                            <VAR name = "Antenna">
                                <VAR name = "GPS_Global">
                                    <SCOPE Class = "Antenna">
                                        <VAR name = "Version">
                                            <STRING>&quot;1.0.1 a&quot;</STRING>
                                        </VAR>
                                        <VAR name = "STKVersion">
                                            <INT>1160</INT>
                                        </VAR>
                                        <VAR name = "ComponentName">
                                            <STRING>&quot;GPS_Global&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Description">
                                            <STRING>&quot;Models a GPS global beam antenna&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;GPS Global&quot;</STRING>
                                        </VAR>
                                        <VAR name = "UserComment">
                                            <STRING>&quot;Models a GPS global beam antenna&quot;</STRING>
                                        </VAR>
                                        <VAR name = "ReadOnly">
                                            <BOOL>false</BOOL>
                                        </VAR>
                                        <VAR name = "Clonable">
                                            <BOOL>true</BOOL>
                                        </VAR>
                                        <VAR name = "Category">
                                            <STRING>&quot;@Top&quot;</STRING>
                                        </VAR>
                                        <VAR name = "DesignFrequency">
                                            <QUANTITY Dimension = "FrequencyUnit" Unit = "Hz">
                                                <REAL>1575000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "BlockType">
                                            <VAR name = "IIF, L1">
                                                <SCOPE Class = "GPSGlobalAntenna">
                                                    <VAR name = "Type">
                                                        <STRING>&quot;IIF, L1&quot;</STRING>
                                                    </VAR>
                                                </SCOPE>
                                            </VAR>
                                        </VAR>
                                        <VAR name = "Efficiency">
                                            <QUANTITY Dimension = "Percent" Unit = "unitValue">
                                                <REAL>0.8</REAL>
                                            </QUANTITY>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "Orientation">
                                <VAR name = "Azimuth Elevation">
                                    <SCOPE Class = "Orientation">
                                        <VAR name = "AzimuthAngle">
                                            <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "ElevationAngle">
                                            <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                <REAL>1.570796326794897</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "AboutBoresight">
                                            <STRING>&quot;Rotate&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Azimuth Elevation&quot;</STRING>
                                        </VAR>
                                        <VAR name = "XPositionOffset">
                                            <QUANTITY Dimension = "SmallDistanceUnit" Unit = "m">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "YPositionOffset">
                                            <QUANTITY Dimension = "SmallDistanceUnit" Unit = "m">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "ZPositionOffset">
                                            <QUANTITY Dimension = "SmallDistanceUnit" Unit = "m">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                        </SCOPE>
                    </VAR>
                    <VAR name = "Mode">
                        <VAR name = "Search_Track">
                            <SCOPE Class = "Bistatic Transmitter Radar Mode">
                                <VAR name = "Version">
                                    <STRING>&quot;1.0.0 a&quot;</STRING>
                                </VAR>
                                <VAR name = "STKVersion">
                                    <INT>1160</INT>
                                </VAR>
                                <VAR name = "ComponentName">
                                    <STRING>&quot;Search_Track&quot;</STRING>
                                </VAR>
                                <VAR name = "Description">
                                    <STRING>&quot;Search Track&quot;</STRING>
                                </VAR>
                                <VAR name = "Type">
                                    <STRING>&quot;Search Track&quot;</STRING>
                                </VAR>
                                <VAR name = "UserComment">
                                    <STRING>&quot;Search Track&quot;</STRING>
                                </VAR>
                                <VAR name = "ReadOnly">
                                    <BOOL>false</BOOL>
                                </VAR>
                                <VAR name = "Clonable">
                                    <BOOL>true</BOOL>
                                </VAR>
                                <VAR name = "Category">
                                    <STRING>&quot;&quot;</STRING>
                                </VAR>
                                <VAR name = "Waveform">
                                    <VAR name = "Continuous Wave">
                                        <SCOPE Class = "SearchTrackBistaticTransmitterWaveform">
                                            <VAR name = "Modulator">
                                                <SCOPE>
                                                    <VAR name = "UseSignalPSD">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                    <VAR name = "PSDLimitMultiplier">
                                                        <INT>15</INT>
                                                    </VAR>
                                                </SCOPE>
                                            </VAR>
                                            <VAR name = "Type">
                                                <STRING>&quot;Continuous Wave&quot;</STRING>
                                            </VAR>
                                        </SCOPE>
                                    </VAR>
                                </VAR>
                            </SCOPE>
                        </VAR>
                    </VAR>
                    <VAR name = "Transmitter">
                        <SCOPE>
                            <VAR name = "Power">
                                <QUANTITY Dimension = "PowerUnit" Unit = "W">
                                    <REAL>31.62277660168379</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "FrequencySpecification">
                                <STRING>&quot;Frequency&quot;</STRING>
                            </VAR>
                            <VAR name = "Wavelength">
                                <QUANTITY Dimension = "SmallDistanceUnit" Unit = "m">
                                    <REAL>0.1903444177777778</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "Frequency">
                                <QUANTITY Dimension = "FrequencyUnit" Unit = "Hz">
                                    <REAL>1575000000</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "PowerAmpBandwidth">
                                <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                    <REAL>30000000</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "AdditionalGainsLosses">
                                <SCOPE>
                                    <VAR name = "GainLossList">
                                        <LIST />
                                    </VAR>
                                </SCOPE>
                            </VAR>
                            <VAR name = "UseFilter">
                                <BOOL>false</BOOL>
                            </VAR>
                            <VAR name = "Filter">
                                <VAR name = "Butterworth">
                                    <SCOPE Class = "Filter">
                                        <VAR name = "Version">
                                            <STRING>&quot;1.0.0 a&quot;</STRING>
                                        </VAR>
                                        <VAR name = "STKVersion">
                                            <INT>1160</INT>
                                        </VAR>
                                        <VAR name = "ComponentName">
                                            <STRING>&quot;Butterworth&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Description">
                                            <STRING>&quot;General form of nth order Butterworth filter with flat passband and stopband regions&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Butterworth&quot;</STRING>
                                        </VAR>
                                        <VAR name = "UserComment">
                                            <STRING>&quot;General form of nth order Butterworth filter with flat passband and stopband regions&quot;</STRING>
                                        </VAR>
                                        <VAR name = "ReadOnly">
                                            <BOOL>false</BOOL>
                                        </VAR>
                                        <VAR name = "Clonable">
                                            <BOOL>true</BOOL>
                                        </VAR>
                                        <VAR name = "Category">
                                            <STRING>&quot;&quot;</STRING>
                                        </VAR>
                                        <VAR name = "LowerBandwidthLimit">
                                            <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                                <REAL>-20000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "UpperBandwidthLimit">
                                            <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                                <REAL>20000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "InsertionLoss">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "Order">
                                            <INT>4</INT>
                                        </VAR>
                                        <VAR name = "CutoffFrequency">
                                            <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                                <REAL>10000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "UsePolarization">
                                <BOOL>true</BOOL>
                            </VAR>
                            <VAR name = "Polarization">
                                <VAR name = "Right-hand Circular">
                                    <SCOPE Class = "Polarization">
                                        <VAR name = "CrossPolLeakage">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1e-06</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Right-hand Circular&quot;</STRING>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "EnableOrthoPolarization">
                                <BOOL>false</BOOL>
                            </VAR>
                        </SCOPE>
                    </VAR>
                    <VAR name = "BistaticReceiverSelectionStrategy">
                        <VAR name = "Static Receiver">
                            <SCOPE Class = "Bistatic Receiver Selection Strategy">
                                <VAR name = "BistaticReceivers">
                                    <SCOPE>
                                        <VAR name = "ObjectList">
                                            <LISTLINKTOOBJ>
                                                <STRING>&quot;Aircraft/Aircraft1/Radar/Radar2&quot;</STRING>
                                            </LISTLINKTOOBJ>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                                <VAR name = "Type">
                                    <STRING>&quot;Static Receiver&quot;</STRING>
                                </VAR>
                                <VAR name = "ComponentName">
                                    <STRING>&quot;Static Receiver&quot;</STRING>
                                </VAR>
                            </SCOPE>
                        </VAR>
                    </VAR>
                </SCOPE>
            </VAR>
        </VAR>
    </SCOPE>
</VAR>
END Radar

BEGIN Extensions

    BEGIN ExternData
    END ExternData

    BEGIN ADFFileData
    END ADFFileData

    BEGIN AccessConstraints
        LineOfSight IncludeIntervals
        RdrXmtTgtAccess IncludeIntervals
    END AccessConstraints

    BEGIN ObjectCoverage
    END ObjectCoverage

    BEGIN Desc
        BEGIN ShortText

        END ShortText
        BEGIN LongText

        END LongText
    END Desc

    BEGIN Refraction
        RefractionModel		 Effective Radius Method

        UseRefractionInAccess		 No

        BEGIN ModelData
            RefractionCeiling		  5.0000000000000000e+03
            MaxTargetAltitude		  1.0000000000000000e+04
            EffectiveRadius		  1.3333333333333299e+00

            UseExtrapolation		 Yes


        END ModelData
    END Refraction

    BEGIN Crdn
    END Crdn

    BEGIN Graphics

        BEGIN Graphics

            ShowRdr		 Yes
            ShowXmtTgt		 No
            ShowXmtRdr		 No
            ShowContour		 No
            UseSinglePulse		 Yes
            LinearSNR		 39.81072
            LineWidth		 1
            LineColor		 #9b30ff
            LineStyle		 1
            XmtTgtWidth		 1
            XmtTgtColor		 #9b30ff
            XmtTgtStyle		 2
            XmtRdrWidth		 1
            XmtRdrColor		 #9b30ff
            XmtRdrStyle		 2
            ShowRdrTgtGrp		 No
            RdrTgtGrpMarker		 0
            RdrTgtGrpColor		 #00ff00
            ShowXmtTgtGrp		 No
            XmtTgtGrpMarker		 0
            XmtTgtGrpColor		 #00ff00
            ShowXmtRdrGrp		 No
            XmtRdrGrpMarker		 0
            XmtRdrGrpColor		 #00ff00
            ShowClutter		 No
            ClutterColor		 #0000ff

            BEGIN Antenna


                BEGIN Graphics

                    ShowGfx		 On
                    Relative		 On
                    ShowBoresight		 On
                    BoresightMarker		 4
                    BoresightColor		 #ffffff

                END Graphics

            END Antenna


        END Graphics
    END Graphics

    BEGIN ContourGfx
        ShowContours		 Off
    END ContourGfx

    BEGIN Contours
        ActiveContourType		 Antenna Gain

        BEGIN ContourSet Antenna Gain
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntAzEl
                    CoordinateSystem		 0
                    BEGIN AzElPatternDef
                        SetResolutionTogether		 0
                        NumAzPoints		 181
                        AzimuthRes		 2
                        MinAzimuth		 -180
                        MaxAzimuth		 180
                        NumElPoints		 91
                        ElevationRes		 1
                        MinElevation		 0
                        MaxElevation		 90
                    END AzElPatternDef
                END CntrAntAzEl
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet EIRP
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntAzEl
                    CoordinateSystem		 0
                    BEGIN AzElPatternDef
                        SetResolutionTogether		 0
                        NumAzPoints		 181
                        AzimuthRes		 2
                        MinAzimuth		 -180
                        MaxAzimuth		 180
                        NumElPoints		 91
                        ElevationRes		 1
                        MinElevation		 0
                        MaxElevation		 90
                    END AzElPatternDef
                END CntrAntAzEl
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet Flux Density
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntLatLon
                    SetResolutionTogether		 true
                    Resolution		 1 1
                    ElevationAngleConstraint		 90
                    BEGIN LatLonSphGrid
                        Centroid		 0 0
                        ConeAngle		 0
                        NumPts		 181 91
                        Altitude		 0
                    END LatLonSphGrid
                END CntrAntLatLon
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet RIP
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntLatLon
                    SetResolutionTogether		 true
                    Resolution		 1 1
                    ElevationAngleConstraint		 90
                    BEGIN LatLonSphGrid
                        Centroid		 0 0
                        ConeAngle		 0
                        NumPts		 181 91
                        Altitude		 0
                    END LatLonSphGrid
                END CntrAntLatLon
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet Spectral Flux Density
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntLatLon
                    SetResolutionTogether		 true
                    Resolution		 1 1
                    ElevationAngleConstraint		 90
                    BEGIN LatLonSphGrid
                        Centroid		 0 0
                        ConeAngle		 0
                        NumPts		 181 91
                        Altitude		 0
                    END LatLonSphGrid
                END CntrAntLatLon
            END ContourDefinition
        END ContourSet
    END Contours

    BEGIN VO
    END VO

    BEGIN 3dVolume
        ActiveVolumeType		 Antenna Beam

        BEGIN VolumeSet Antenna Beam
            Scale		 4
            MinimumDisplayedGain		 1
            Frequency		 2997924580
            ShowAsWireframe		 0
            CoordinateSystem		 0
            BEGIN AzElPatternDef
                SetResolutionTogether		 0
                NumAzPoints		 181
                AzimuthRes		 2
                MinAzimuth		 -180
                MaxAzimuth		 180
                NumElPoints		 91
                ElevationRes		 1
                MinElevation		 0
                MaxElevation		 90
            END AzElPatternDef
            ColorMethod		 1
            MinToMaxStartColor		 #ff0000
            MinToMaxStopColor		 #0000ff
            RelativeToMaximum		 0
        END VolumeSet
        BEGIN VolumeGraphics
            ShowContours		 No
            ShowVolume		 No
        END VolumeGraphics
    END 3dVolume

END Extensions
