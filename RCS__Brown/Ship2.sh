stk.v.11.0
WrittenBy    STK_v11.6.0

BEGIN Ship

    Name		 Ship2

    BEGIN VehiclePath
        CentralBody		 Earth

        BEGIN Interval

            StartTime		 18 Oct 2020 01:00:00.000000000
            StopTime		 18 Oct 2020 01:03:48.383537356

        END Interval

        StoreEphemeris		 Yes
        SmoothInterp		 No

        BEGIN GreatArc

            VersionIndicator		 20071204
            Method		 DetTimeAccFromVel

            ArcSmartInterval		
            BEGIN EVENTINTERVAL
                StartEvent		
                BEGIN EVENT
                    Epoch		 18 Oct 2020 01:00:00.000000000
                    EventEpoch		
                    BEGIN EVENT
                        Type		 EVENT_LINKTO
                        Name		 AnalysisStartTime
                        AbsolutePath		 Scenario
                    END EVENT
                    EpochState		 Implicit
                END EVENT
                StopEvent		
                BEGIN EVENT
                    Epoch		 18 Oct 2020 01:03:48.383537356
                    EpochState		 Explicit
                END EVENT
                IntervalState		 StartStop
            END EVENTINTERVAL

            TimeOfFirstWaypoint		 18 Oct 2020 01:00:00.000000000

            UseScenTime		 No
            ArcGranularity		  8.9992440604777096e-03
            DefaultRate		  1.5433332999999999e+01
            DefaultAltitude		  0.0000000000000000e+00
            DefaultTurnRadius		  0.0000000000000000e+00
            AltRef		 MSL
            AltInterpMethod		 MSLHeight
            NumberOfWaypoints		 2

            BEGIN Waypoints

                 0.0000000000000000e+00  5.6508605400000000e+00 -1.6660465554000001e+02  0.0000000000000000e+00  2.5000000000000000e+02  0.0000000000000000e+00
                 2.2838353735568353e+02  5.6132505400000001e+00 -1.6609065228000000e+02  0.0000000000000000e+00  2.5000000000000000e+02  0.0000000000000000e+00

            END Waypoints

        END GreatArc

    END VehiclePath

    BEGIN Ephemeris

        NumberOfEphemerisPoints		 58

        ScenarioEpoch		 18 Oct 2020 01:00:00.000000

# Epoch in JDate format: 2459140.54166666666667
# Epoch in YYDDD format:   20292.04166666666667


        InterpolationMethod		 GreatArcMSL

        InterpolationSamplesM1		 1

        CentralBody		 Earth

        CoordinateSystem		 Fixed

        BlockingFactor		 20

# Time of first point: 18 Oct 2020 01:00:00.000000000 UTCG = 2459140.54166666666667 JDate = 20292.04166666666667 YYDDD

        EphemerisTimePosVel		

 0.0000000000000000e+00 -6.1746792236465421e+06 -1.4704859004161898e+06  6.2384888996556588e+05  5.6029082150543189e+01 -2.4297376252335752e+02 -1.8013680318322798e+01
 4.0067281616036832e+00 -6.1744546537235174e+06 -1.4714594141457165e+06  6.2377670616259368e+05  5.6067094490152826e+01 -2.4296470908320779e+02 -1.8017520793507284e+01
 8.0134563331686959e+00 -6.1742299314969508e+06 -1.4724328915922772e+06  6.2370450697246264e+05  5.6105105446336992e+01 -2.4295564965007787e+02 -1.8021360824422096e+01
 1.2020184514695224e+01 -6.1740050569723863e+06 -1.4734063327318616e+06  6.2363229239695298e+05  5.6143115022075335e+01 -2.4294658422328709e+02 -1.8025200410960615e+01
 1.6026912706187471e+01 -6.1737800301553560e+06 -1.4743797375404588e+06  6.2356006243784504e+05  5.6181123213464836e+01 -2.4293751280373962e+02 -1.8029039553074185e+01
 2.0033640907637704e+01 -6.1735548510514107e+06 -1.4753531059940481e+06  6.2348781709692045e+05  5.6219130020457314e+01 -2.4292843539145932e+02 -1.8032878250586901e+01
 2.4040369119053103e+01 -6.1733295196660934e+06 -1.4763264380686237e+06  6.2341555637596000e+05  5.6257135441921768e+01 -2.4291935198672215e+02 -1.8036716503295370e+01
 2.8047097340431513e+01 -6.1731040360049577e+06 -1.4772997337401735e+06  6.2334328027674614e+05  5.6295139478021973e+01 -2.4291026258946945e+02 -1.8040554311469101e+01
 3.2053825571770233e+01 -6.1728784000735581e+06 -1.4782729929846830e+06  6.2327098880106048e+05  5.6333142126022942e+01 -2.4290116720036224e+02 -1.8044391674727340e+01
 3.6060553813072083e+01 -6.1726526118774554e+06 -1.4792462157781492e+06  6.2319868195068568e+05  5.6371143385901988e+01 -2.4289206581941932e+02 -1.8048228592882442e+01
 4.0067282064336410e+01 -6.1724266714222124e+06 -1.4802194020965586e+06  6.2312635972740524e+05  5.6409143256444096e+01 -2.4288295844690910e+02 -1.8052065066108163e+01
 4.4074010325563947e+01 -6.1722005787133947e+06 -1.4811925519159071e+06  6.2305402213300182e+05  5.6447141759602580e+01 -2.4287384509168896e+02 -1.8055900906618021e+01
 4.8080738596751210e+01 -6.1719743348786132e+06 -1.4821656654816375e+06  6.2298166928327817e+05  5.6484429278651781e+01 -2.4286489614997055e+02 -1.8059664092363544e+01
 5.2087466877905172e+01 -6.1717479405269884e+06 -1.4831387429153435e+06  6.2290930124131753e+05  5.6522422712965295e+01 -2.4285577146607122e+02 -1.8063499004241379e+01
 5.6094195169019038e+01 -6.1715213939474784e+06 -1.4841117837810756e+06  6.2283691783445084e+05  5.6560414753654953e+01 -2.4284664079241401e+02 -1.8067333470514129e+01
 6.0100923470096163e+01 -6.1712946951456675e+06 -1.4850847880548327e+06  6.2276451906446391e+05  5.6598405400677784e+01 -2.4283750412898405e+02 -1.8071167491509435e+01
 6.4107651781135232e+01 -6.1710678441271428e+06 -1.4860577557126179e+06  6.2269210493314103e+05  5.6636394651562874e+01 -2.4282836147641498e+02 -1.8075001066444376e+01
 6.8114380102136224e+01 -6.1708408408974949e+06 -1.4870306867304349e+06  6.2261967544226884e+05  5.6674382505937089e+01 -2.4281921283474358e+02 -1.8078834195993828e+01
 7.2121108433100602e+01 -6.1706136854623193e+06 -1.4880035810842875e+06  6.2254723059363209e+05  5.6712368965040831e+01 -2.4281005820371678e+02 -1.8082666879659353e+01
 7.6127836774024772e+01 -6.1703863778272122e+06 -1.4889764387501774e+06  6.2247477038901800e+05  5.6750354024205102e+01 -2.4280089758443086e+02 -1.8086499117355984e+01
 8.0134565124916577e+01 -6.1701589179977784e+06 -1.4899492597041132e+06  6.2240229483021272e+05  5.6788337685504864e+01 -2.4279173097640179e+02 -1.8090330909068275e+01
 8.4141293485768969e+01 -6.1699313059796197e+06 -1.4909220439221009e+06  6.2232980391900335e+05  5.6826319946142355e+01 -2.4278255838029511e+02 -1.8094162254640146e+01
 8.8148021856581948e+01 -6.1697035417783512e+06 -1.4918947913801474e+06  6.2225729765717755e+05  5.6864300804950652e+01 -2.4277337979639273e+02 -1.8097993153953240e+01
 9.2154750237358968e+01 -6.1694756253995849e+06 -1.4928675020542603e+06  6.2218477604652324e+05  5.6902280264237682e+01 -2.4276419522412829e+02 -1.8101823607345711e+01
 9.6161478628098763e+01 -6.1692475568489349e+06 -1.4938401759204501e+06  6.2211223908882763e+05  5.6940258320544970e+01 -2.4275500466437890e+02 -1.8105653613912313e+01
 1.0016820702880113e+02 -6.1690193361320207e+06 -1.4948128129547257e+06  6.2203968678588059e+05  5.6978234971263092e+01 -2.4274580811771378e+02 -1.8109483174227062e+01
 1.0417493543946622e+02 -6.1687909632544732e+06 -1.4957854131330978e+06  6.2196711913947028e+05  5.7016210218297523e+01 -2.4273660558371668e+02 -1.8113312287874010e+01
 1.0818166386009324e+02 -6.1685624382219166e+06 -1.4967579764315803e+06  6.2189453615138622e+05  5.7054184059714132e+01 -2.4272739706284366e+02 -1.8117140954822499e+01
 1.1218839229068428e+02 -6.1683337610399863e+06 -1.4977305028261836e+06  6.2182193782341864e+05  5.7092156495341747e+01 -2.4271818255513142e+02 -1.8120969175110787e+01
 1.1619512073123586e+02 -6.1681049317143122e+06 -1.4987029922929220e+06  6.2174932415735675e+05  5.7130127522762066e+01 -2.4270896206115955e+02 -1.8124796948588379e+01
 1.2020184918175286e+02 -6.1678759502505399e+06 -1.4996754448078107e+06  6.2167669515499112e+05  5.7168097142083823e+01 -2.4269973558092690e+02 -1.8128624274926416e+01
 1.2420857764223030e+02 -6.1676468166543096e+06 -1.5006478603468661e+06  6.2160405081811314e+05  5.7206065351337074e+01 -2.4269050311487851e+02 -1.8132451154371541e+01
 1.2821530611267036e+02 -6.1674175309312707e+06 -1.5016202388861037e+06  6.2153139114851342e+05  5.7244032151742381e+01 -2.4268126466275589e+02 -1.8136277586531278e+01
 1.3222203459307494e+02 -6.1671880930870725e+06 -1.5025925804015421e+06  6.2145871614798449e+05  5.7281997540245385e+01 -2.4267202022525737e+02 -1.8140103571689473e+01
 1.3622876308344280e+02 -6.1669585031273682e+06 -1.5035648848691967e+06  6.2138602581831685e+05  5.7319961516847357e+01 -2.4266276980242620e+02 -1.8143929109260174e+01
 1.4023549158377375e+02 -6.1667287610578183e+06 -1.5045371522650889e+06  6.2131332016130444e+05  5.7357924080115112e+01 -2.4265351339457212e+02 -1.8147754199623531e+01
 1.4424222009406571e+02 -6.1664988668840835e+06 -1.5055093825652376e+06  6.2124059917873878e+05  5.7395885229395986e+01 -2.4264425100189106e+02 -1.8151578842219152e+01
 1.4824894861432284e+02 -6.1662688206118327e+06 -1.5064815757456662e+06  6.2116786287241394e+05  5.7433844963966102e+01 -2.4263498262452680e+02 -1.8155403037406483e+01
 1.5225567714454016e+02 -6.1660386222467301e+06 -1.5074537317823945e+06  6.2109511124412250e+05  5.7471803281708077e+01 -2.4262570826301055e+02 -1.8159226784781755e+01
 1.5626240568472321e+02 -6.1658082717944579e+06 -1.5084258506514444e+06  6.2102234429565922e+05  5.7509760169119062e+01 -2.4261642791032665e+02 -1.8163050220810494e+01
 1.6026913423487073e+02 -6.1655777684443966e+06 -1.5093979321290047e+06  6.2094956194605294e+05  5.7548017947991596e+01 -2.4260706757432447e+02 -1.8166903788064388e+01
 1.6427586279497618e+02 -6.1653471126300972e+06 -1.5103699762955126e+06  6.2087676424049749e+05  5.7585968778017879e+01 -2.4259777600308868e+02 -1.8170725859259548e+01
 1.6828259136504795e+02 -6.1651163047586782e+06 -1.5113419832252010e+06  6.2080395122149203e+05  5.7623918187884307e+01 -2.4258847844960667e+02 -1.8174547482144565e+01
 1.7228931994508318e+02 -6.1648853448358271e+06 -1.5123139528941109e+06  6.2073112289083190e+05  5.7661866174509498e+01 -2.4257917491460978e+02 -1.8178368656733287e+01
 1.7629604853508178e+02 -6.1646542328672456e+06 -1.5132858852782736e+06  6.2065827925031306e+05  5.7699812742327381e+01 -2.4256986539702524e+02 -1.8182189383278740e+01
 1.8030277713504307e+02 -6.1644229688586220e+06 -1.5142577803537233e+06  6.2058542030173133e+05  5.7737757884796693e+01 -2.4256054989843832e+02 -1.8186009661380353e+01
 1.8430950574496904e+02 -6.1641915528156627e+06 -1.5152296380964965e+06  6.2051254604688252e+05  5.7775701602170479e+01 -2.4255122841881547e+02 -1.8189829490695082e+01
 1.8831623436485737e+02 -6.1639599847440803e+06 -1.5162014584826331e+06  6.2043965648756502e+05  5.7813643897659759e+01 -2.4254190095734472e+02 -1.8193648871849931e+01
 1.9232296299470900e+02 -6.1637282646495746e+06 -1.5171732414881687e+06  6.2036675162557478e+05  5.7851584764326944e+01 -2.4253256751573855e+02 -1.8197467804054654e+01
 1.9632969163452222e+02 -6.1634963925378658e+06 -1.5181449870891448e+06  6.2029383146270982e+05  5.7889524205155595e+01 -2.4252322809325753e+02 -1.8201286287691364e+01
 2.0033642028430046e+02 -6.1632643684146693e+06 -1.5191166952615983e+06  6.2022089600076783e+05  5.7927462218079711e+01 -2.4251388269043937e+02 -1.8205104322160945e+01
 2.0434314894404511e+02 -6.1630321922857063e+06 -1.5200883659815730e+06  6.2014794524154800e+05  5.7965398802600632e+01 -2.4250453130736784e+02 -1.8208921907936482e+01
 2.0834987761374978e+02 -6.1627998641566988e+06 -1.5210599992251107e+06  6.2007497918684816e+05  5.8003333955480706e+01 -2.4249517394486170e+02 -1.8212739044423351e+01
 2.1235660629342004e+02 -6.1625673840333810e+06 -1.5220315949682542e+06  6.2000199783846794e+05  5.8041267680594181e+01 -2.4248581060194539e+02 -1.8216555732275019e+01
 2.1636333498305427e+02 -6.1623347519214796e+06 -1.5230031531870421e+06  6.1992900119820575e+05  5.8079199972726670e+01 -2.4247644127995463e+02 -1.8220371970317473e+01
 2.2037006368265034e+02 -6.1621019678267306e+06 -1.5239746738575280e+06  6.1985598926786275e+05  5.8117130830801791e+01 -2.4246706597910708e+02 -1.8224187759096331e+01
 2.2437679239221373e+02 -6.1618690317548811e+06 -1.5249461569557514e+06  6.1978296204923920e+05  5.8155060258837672e+01 -2.4245768469841667e+02 -1.8228003098917000e+01
 2.2838353735568353e+02 -6.1616359427663796e+06 -1.5259176063960860e+06  6.1970991924797674e+05  5.8192988376110151e+01 -2.4244829740813807e+02 -1.8231818004601653e+01


    END Ephemeris

    BEGIN MassProperties

        Mass		  1.0000000000000000e+03
        InertiaXX		  4.5000000000000000e+03
        InertiaYX		  0.0000000000000000e+00
        InertiaYY		  4.5000000000000000e+03
        InertiaZX		  0.0000000000000000e+00
        InertiaZY		  0.0000000000000000e+00
        InertiaZZ		  4.5000000000000000e+03

    END MassProperties

    BEGIN Attitude

        ScenarioEpoch		 18 Oct 2020 01:00:00.000000

        BEGIN Profile
            Name		 AircraftAtt
            UserNamed		 No
            StartTime		  0.0000000000000000e+00
            BEGIN ECFVelRadial
                Azimuth		  0.0000000000000000e+00
            END ECFVelRadial
        END Profile

    END Attitude

    BEGIN Swath

        SwathType		 ElevAngle
        ElevationAngle		  0.0000000000000000e+00
        HalfAngle		  0.0000000000000000e+00
        Distance		  0.0000000000000000e+00
        RepType		 NoSwath

    END Swath

    BEGIN Eclipse

        Sunlight		 Off
        SunlightLineStyle		 0
        SunlightLineWidth		 3
        SunlightMarkerStyle		 19

        Penumbra		 Off
        PenumbraLineStyle		 0
        PenumbraLineWidth		 3
        PenumbraMarkerStyle		 19

        Umbra		 Off
        UmbraLineStyle		 0
        UmbraLineWidth		 3
        UmbraMarkerStyle		 19

        SunlightPenumbraLine		 Off
        PenumbraUmbraLine		 Off

        SunlightColor		 #ffff00
        PenumbraColor		 #87cefa
        UmbraColor		 #0000ff
        UseCustomEclipseBodies		 No

    END Eclipse

    BEGIN RealTimeDef

        HistoryDuration		  1.8000000000000000e+03
        LookAheadDuration		  1.8000000000000000e+03

    END RealTimeDef


    BEGIN LineOfSightModel

        ModelType		 CbShape
        HeightAboveSurface		  0.0000000000000000e+00

    END LineOfSightModel


    BEGIN Extensions

        BEGIN LaserCAT
        END LaserCAT

        BEGIN ExternData
        END ExternData

        BEGIN RFI
        END RFI

        BEGIN ADFFileData
        END ADFFileData

        BEGIN AccessConstraints
            LineOfSight IncludeIntervals
        END AccessConstraints

        BEGIN ObjectCoverage
        END ObjectCoverage

        BEGIN Desc
            BEGIN ShortText

            END ShortText
            BEGIN LongText

            END LongText
        END Desc

        BEGIN Atmosphere
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Atmosphere_Extension">
    <SCOPE Class = "AtmosphereExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Atmosphere_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "InheritAtmosAbsorptionModel">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "AtmosAbsorptionModel">
            <VAR name = "Simple_Satcom">
                <SCOPE Class = "AtmosphericAbsorptionModel">
                    <VAR name = "Version">
                        <STRING>&quot;1.0.1 a&quot;</STRING>
                    </VAR>
                    <VAR name = "STKVersion">
                        <INT>1160</INT>
                    </VAR>
                    <VAR name = "ComponentName">
                        <STRING>&quot;Simple_Satcom&quot;</STRING>
                    </VAR>
                    <VAR name = "Description">
                        <STRING>&quot;Simple Satcom gaseous absorption model&quot;</STRING>
                    </VAR>
                    <VAR name = "Type">
                        <STRING>&quot;Simple Satcom&quot;</STRING>
                    </VAR>
                    <VAR name = "UserComment">
                        <STRING>&quot;Simple Satcom gaseous absorption model&quot;</STRING>
                    </VAR>
                    <VAR name = "ReadOnly">
                        <BOOL>false</BOOL>
                    </VAR>
                    <VAR name = "Clonable">
                        <BOOL>true</BOOL>
                    </VAR>
                    <VAR name = "Category">
                        <STRING>&quot;&quot;</STRING>
                    </VAR>
                    <VAR name = "SurfaceTemperature">
                        <QUANTITY Dimension = "Temperature" Unit = "K">
                            <REAL>293.15</REAL>
                        </QUANTITY>
                    </VAR>
                    <VAR name = "WaterVaporConcentration">
                        <QUANTITY Dimension = "Density" Unit = "g*m^-3">
                            <REAL>7.5</REAL>
                        </QUANTITY>
                    </VAR>
                </SCOPE>
            </VAR>
        </VAR>
        <VAR name = "EnableLocalRainData">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "LocalRainIsoHeight">
            <QUANTITY Dimension = "DistanceUnit" Unit = "m">
                <REAL>2000</REAL>
            </QUANTITY>
        </VAR>
        <VAR name = "LocalRainRate">
            <QUANTITY Dimension = "SlowRate" Unit = "mm*hr^-1">
                <REAL>1</REAL>
            </QUANTITY>
        </VAR>
        <VAR name = "LocalSurfaceTemp">
            <QUANTITY Dimension = "Temperature" Unit = "K">
                <REAL>293.15</REAL>
            </QUANTITY>
        </VAR>
    </SCOPE>
</VAR>        END Atmosphere

        BEGIN RadarCrossSection
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_RCS_Extension">
    <SCOPE Class = "RadarRCSExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_RCS_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Inherit">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Model">
            <VAR name = "Radar_Cross_Section">
                <SCOPE Class = "RCS">
                    <VAR name = "Version">
                        <STRING>&quot;1.0.0 a&quot;</STRING>
                    </VAR>
                    <VAR name = "STKVersion">
                        <INT>1160</INT>
                    </VAR>
                    <VAR name = "ComponentName">
                        <STRING>&quot;Radar_Cross_Section&quot;</STRING>
                    </VAR>
                    <VAR name = "Description">
                        <STRING>&quot;Radar Cross Section&quot;</STRING>
                    </VAR>
                    <VAR name = "Type">
                        <STRING>&quot;Radar Cross Section&quot;</STRING>
                    </VAR>
                    <VAR name = "UserComment">
                        <STRING>&quot;Radar Cross Section&quot;</STRING>
                    </VAR>
                    <VAR name = "ReadOnly">
                        <BOOL>false</BOOL>
                    </VAR>
                    <VAR name = "Clonable">
                        <BOOL>true</BOOL>
                    </VAR>
                    <VAR name = "Category">
                        <STRING>&quot;&quot;</STRING>
                    </VAR>
                    <VAR name = "FrequencyBandList">
                        <LIST>
                            <SCOPE>
                                <VAR name = "MinFrequency">
                                    <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                        <REAL>2997920</REAL>
                                    </QUANTITY>
                                </VAR>
                                <VAR name = "ComputeTypeStrategy">
                                    <VAR name = "External File">
                                        <SCOPE Class = "RCS Compute Strategy">
                                            <VAR name = "Filename">
                                                <STRING>
                                                    <PROP name = "FullName">
                                                        <STRING>&quot;C:\Users\PhD Standard User\Documents\STK 11 (x64)\Testrcs.rcs&quot;</STRING>
                                                    </PROP>&quot;C:\Users\PhD Standard User\Documents\STK 11 (x64)\Testrcs.rcs&quot;</STRING>
                                            </VAR>
                                            <VAR name = "Type">
                                                <STRING>&quot;External File&quot;</STRING>
                                            </VAR>
                                            <VAR name = "ComponentName">
                                                <STRING>&quot;External File&quot;</STRING>
                                            </VAR>
                                        </SCOPE>
                                    </VAR>
                                </VAR>
                                <VAR name = "SwerlingCase">
                                    <STRING>&quot;0&quot;</STRING>
                                </VAR>
                            </SCOPE>
                        </LIST>
                    </VAR>
                </SCOPE>
            </VAR>
        </VAR>
    </SCOPE>
</VAR>        END RadarCrossSection

        BEGIN RadarClutter
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_Clutter_Extension">
    <SCOPE Class = "RadarClutterExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_Clutter_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Inherit">
            <BOOL>true</BOOL>
        </VAR>
    </SCOPE>
</VAR>        END RadarClutter

        BEGIN Ellipse
            TimesTrackVehStartTime		 Yes
        END Ellipse

        BEGIN Crdn
        END Crdn

        BEGIN Graphics

            BEGIN GenericGraphics
                IntvlHideShowAll		 Off
                ShowPassLabel		 Off
                ShowPathLabel		 Off
                TransformTrajectory		 On
                MinGfxGndTrkTimeStep		  0.0000000000000000e+00
                MaxGfxGndTrkTimeStep		  6.0000000000000000e+02
                MinGfxOrbitTimeStep		  0.0000000000000000e+00
                MaxGfxOrbitTimeStep		  6.0000000000000000e+02
                ShowGlintPoint		 Off
                ShowGlintColor		 #ffffff
                ShowGlintStyle		 2
            END GenericGraphics

            BEGIN AttributeData
                ShowGfx		 On
                AttributeType		                Basic		
                BEGIN DefaultAttributes
                    Show		 On
                    Inherit		 On
                    ShowLabel		 On
                    ShowGndTrack		 On
                    ShowGndMarker		 On
                    ShowOrbit		 On
                    ShowOrbitMarker		 On
                    ShowElsetNum		 Off
                    ShowSpecialSwath		 On
                    MarkerColor		 #00ff00
                    GroundTrackColor		 #00ff00
                    SwathColor		 #00ff00
                    LabelColor		 #00ff00
                    LineStyle		 0
                    LineWidth		 1
                    MarkerStyle		 19
                    FontStyle		 0
                    SwathLineStyle		 0
                    SpecSwathLineStyle		 1
                END DefaultAttributes

                BEGIN CustomIntervalList
                    BEGIN DefaultAttributes
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 19
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DefaultAttributes
                END CustomIntervalList

                BEGIN AccessIntervalsAttributes
                    BEGIN AttrDuringAccess
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END AttrDuringAccess
                    BEGIN AttrDuringNoAccess
                        Show		 Off
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ffff
                        GroundTrackColor		 #00ffff
                        SwathColor		 #00ffff
                        LabelColor		 #00ffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END AttrDuringNoAccess
                END AccessIntervalsAttributes

                BEGIN TimeComponentIntervalsAttributes
                    BEGIN DefaultAttributes
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DefaultAttributes
                END TimeComponentIntervalsAttributes

                BEGIN RealTimeAttributes
                    BEGIN HistoryAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END HistoryAttr
                    BEGIN SplineAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ffff00
                        GroundTrackColor		 #ffff00
                        SwathColor		 #ffff00
                        LabelColor		 #ffff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END SplineAttr
                    BEGIN LookAheadAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ffffff
                        GroundTrackColor		 #ffffff
                        SwathColor		 #ffffff
                        LabelColor		 #ffffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END LookAheadAttr
                    BEGIN DropOutAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ff0000
                        GroundTrackColor		 #ff0000
                        SwathColor		 #ff0000
                        LabelColor		 #ff0000
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DropOutAttr
                END RealTimeAttributes
            END AttributeData

            BEGIN LeadTrailData
                GtLeadingType		 AllData
                GtTrailingType		 AllData
                OrbitLeadingType		 AllData
                OrbitTrailingType		 OnePass
            END LeadTrailData
            BEGIN SaaData
                ShowSaa		 Off
                ShowSaaFill		 Off
                SaaFillTranslucency		 0.7
                TrackSaa		 On
                SaaAltitude		 500000
            END SaaData
            BEGIN GroundTracks
                BEGIN GroundTrack
                    CentralBody		 Earth
                END GroundTrack
            END GroundTracks
            BEGIN WaypointData
                ShowWayptMarkers		 On
                ShowWayptTurnMarkers		 On
                ShowWayptMarkersExtEphem		 Off
                NewWayptMarkerShow		 On
                NewWayptShowLabel		 Off
                NewWayptMarkerStyle		 3
                WayptShow		 On
                WayptShowLabel		 Off
                WayptMarkerStyle		 3
            END WaypointData
            BEGIN EllipseSetGxData
                BEGIN DefEllipseSetGx
                    ShowStatic		 On
                    ShowDynamic		 On
                    UseLastDynPos		 Off
                    HoldLastDynPos		 Off
                    ShowName		 Off
                    ShowMarker		 On
                    MarkerStyle		 0
                    LineStyle		 0
                    LineWidth		 1
                END DefEllipseSetGx
            END EllipseSetGxData
        END Graphics

        BEGIN ContourGfx
            ShowContours		 Off
        END ContourGfx

        BEGIN Contours
            ActiveContourType		 Radar Cross Section

            BEGIN ContourSet Radar Cross Section
                Altitude		 0
                ShowAtAltitude		 Off
                Projected		 On
                Relative		 On
                ShowLabels		 Off
                LineWidth		 1
                DecimalDigits		 1
                ColorRamp		 On
                ColorRampStartColor		 #ff0000
                ColorRampEndColor		 #0000ff
                BEGIN ContourDefinition
                    BEGIN CntrAntAzEl
                        CoordinateSystem		 0
                        BEGIN AzElPatternDef
                            SetResolutionTogether		 0
                            NumAzPoints		 361
                            AzimuthRes		 1
                            MinAzimuth		 -180
                            MaxAzimuth		 180
                            NumElPoints		 91
                            ElevationRes		 1
                            MinElevation		 0
                            MaxElevation		 90
                        END AzElPatternDef
                    END CntrAntAzEl
                    BEGIN RCSContour
                        Frequency		 2997924580
                        ComputeType		 0
                    END RCSContour
                END ContourDefinition
            END ContourSet
        END Contours

        BEGIN VO
        END VO

        BEGIN 3dVolume
            ActiveVolumeType		 Radar Cross Section

            BEGIN VolumeSet Radar Cross Section
                Scale		 100
                MinimumDisplayedRcs		 1
                Frequency		  1.4500000000000000e+10
                ShowAsWireframe		 1
                BEGIN AzElPatternDef
                    SetResolutionTogether		 0
                    NumAzPoints		 50
                    AzimuthRes		 7.346938775510203
                    MinAzimuth		 -180
                    MaxAzimuth		 180
                    NumElPoints		 50
                    ElevationRes		 3.673469387755102
                    MinElevation		 0
                    MaxElevation		 180
                END AzElPatternDef
                ColorMethod		 1
                MinToMaxStartColor		 #ff0000
                MinToMaxStopColor		 #0000ff
                RelativeToMaximum		 0
            END VolumeSet
            BEGIN VolumeGraphics
                ShowContours		 No
                ShowVolume		 Yes
            END VolumeGraphics
        END 3dVolume

    END Extensions

    BEGIN SubObjects

    END SubObjects

END Ship

