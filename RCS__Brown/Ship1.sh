stk.v.11.0
WrittenBy    STK_v11.6.0

BEGIN Ship

    Name		 Ship1

    BEGIN VehiclePath
        CentralBody		 Earth

        BEGIN Interval

            StartTime		 18 Oct 2020 01:00:00.000000000
            StopTime		 18 Oct 2020 01:02:16.338354688

        END Interval

        StoreEphemeris		 Yes
        SmoothInterp		 No

        BEGIN GreatArc

            VersionIndicator		 20071204
            Method		 DetTimeAccFromVel

            ArcSmartInterval		
            BEGIN EVENTINTERVAL
                BEGIN Interval
                    Start		 18 Oct 2020 01:00:00.000000000
                    Stop		 18 Oct 2020 01:02:16.338354688
                END Interval
                IntervalState		 Explicit
            END EVENTINTERVAL

            TimeOfFirstWaypoint		 18 Oct 2020 01:00:00.000000000

            UseScenTime		 No
            ArcGranularity		  8.9992440604777096e-03
            DefaultRate		  1.5433332999999999e+01
            DefaultAltitude		  0.0000000000000000e+00
            DefaultTurnRadius		  0.0000000000000000e+00
            AltRef		 MSL
            AltInterpMethod		 MSLHeight
            NumberOfWaypoints		 2

            BEGIN Waypoints

                 0.0000000000000000e+00  5.1828250400000000e+00 -1.6605722116999999e+02  0.0000000000000000e+00  2.5000000000000000e+02  0.0000000000000000e+00
                 1.3633835468841394e+02  5.4711683400000002e+00 -1.6616587226999999e+02  0.0000000000000000e+00  2.5000000000000000e+02  0.0000000000000000e+00

            END Waypoints

        END GreatArc

    END VehiclePath

    BEGIN Ephemeris

        NumberOfEphemerisPoints		 36

        ScenarioEpoch		 18 Oct 2020 01:00:00.000000

# Epoch in JDate format: 2459140.54166666666667
# Epoch in YYDDD format:   20292.04166666666667


        InterpolationMethod		 GreatArcMSL

        InterpolationSamplesM1		 1

        CentralBody		 Earth

        CoordinateSystem		 Fixed

        BlockingFactor		 20

# Time of first point: 18 Oct 2020 01:00:00.000000000 UTCG = 2459140.54166666666667 JDate = 20292.04166666666667 YYDDD

        EphemerisTimePosVel		

 0.0000000000000000e+00 -6.1650903700355273e+06 -1.5305914824527041e+06  5.7232279368092015e+05 -7.7327845531920891e-01  9.0801005361681774e+01  2.3292612448100016e+02
 3.8953855440788052e+00 -6.1650933099554572e+06 -1.5302377588132541e+06  5.7323012632268714e+05 -7.3617040949954782e-01  9.0810209231066253e+01  2.3292265658913081e+02
 7.7907709739213082e+00 -6.1650961053246912e+06 -1.5298839993357747e+06  5.7413744541898009e+05 -6.9906235801014027e-01  9.0819410955600389e+01  2.3291918320631595e+02
 1.1686156289520451e+01 -6.1650987561432123e+06 -1.5295302040286227e+06  5.7504475094841188e+05 -6.6195429923665572e-01  9.0828610537122756e+01  2.3291570433184916e+02
 1.5581541490870602e+01 -6.1651012624110077e+06 -1.5291763729001486e+06  5.7595204288959503e+05 -6.2484623763256331e-01  9.0837807974561301e+01  2.3291221996614101e+02
 1.9476926577965465e+01 -6.1651036241280697e+06 -1.5288225059587054e+06  5.7685932122114231e+05 -5.8773817086601732e-01  9.0847003266946231e+01  2.3290873010958171e+02
 2.3372311550799271e+01 -6.1651058412943929e+06 -1.5284686032126527e+06  5.7776658592166658e+05 -5.5063010098331544e-01  9.0856196415568974e+01  2.3290523476166749e+02
 2.7267696409364387e+01 -6.1651079139099754e+06 -1.5281146646703426e+06  5.7867383696978178e+05 -5.1352203094857785e-01  9.0865387419335718e+01  2.3290173392282369e+02
 3.1163081153656403e+01 -6.1651098419748219e+06 -1.5277606903401313e+06  5.7958107434410171e+05 -4.7641397439517119e-01  9.0874576337606612e+01  2.3289822756986686e+02
 3.5058465783670357e+01 -6.1651116243246710e+06 -1.5274066799419345e+06  5.8048829791287833e+05 -4.3895447772789914e-01  9.0883850127907010e+01  2.3289468243155451e+02
 3.8953850299398020e+01 -6.1651132619270198e+06 -1.5270526337239442e+06  5.8139550774606213e+05 -4.0185046595013141e-01  9.0893033668976429e+01  2.3289116545428115e+02
 4.2849234700835176e+01 -6.1651147549944762e+06 -1.5266985517472047e+06  5.8230270384239580e+05 -3.6474645758921376e-01  9.0902215064991964e+01  2.3288764298755663e+02
 4.6744618987972956e+01 -6.1651161035270607e+06 -1.5263444340200732e+06  5.8320988618050097e+05 -3.2764244991366603e-01  9.0911394315373187e+01  2.3288411503161774e+02
 5.0640003160808440e+01 -6.1651173075247882e+06 -1.5259902805509088e+06  5.8411705473900179e+05 -2.9053844784003696e-01  9.0920571420839522e+01  2.3288058158618196e+02
 5.4535387219332847e+01 -6.1651183669876885e+06 -1.5256360913480679e+06  5.8502420949652034e+05 -2.5343444935301906e-01  9.0929746380551492e+01  2.3287704265158573e+02
 5.8430771163542857e+01 -6.1651192819157867e+06 -1.5252818664199098e+06  5.8593135043168080e+05 -2.1633045491364220e-01  9.0938919192991222e+01  2.3287349822842631e+02
 6.2326154993428737e+01 -6.1651200523091089e+06 -1.5249276057748015e+06  5.8683847752310638e+05 -1.7922646791170749e-01  9.0948089860365542e+01  2.3286994831584445e+02
 6.6221538708987296e+01 -6.1651206781676942e+06 -1.5245733094210990e+06  5.8774559074942186e+05 -1.4212248844146289e-01  9.0957258380556169e+01  2.3286639291467333e+02
 7.0116922310211962e+01 -6.1651211594915790e+06 -1.5242189773671674e+06  5.8865269008925138e+05 -1.0501851535571051e-01  9.0966424754192076e+01  2.3286283202467354e+02
 7.4012305797095351e+01 -6.1651214962808015e+06 -1.5238646096213707e+06  5.8955977552121971e+05 -6.7914552717913038e-02  9.0975588980678481e+01  2.3285926564608113e+02
 7.7907689169632675e+01 -6.1651216885354090e+06 -1.5235102061920725e+06  5.9046684702395229e+05 -3.0810598797631455e-02  9.0984751060339903e+01  2.3285569377877573e+02
 8.1803072427816346e+01 -6.1651217362554492e+06 -1.5231557670876377e+06  5.9137390457607503e+05  6.2933448795407434e-03  9.0993910992611404e+01  2.3285211642298322e+02
 8.5698455571642441e+01 -6.1651216394409686e+06 -1.5228012923164321e+06  5.9228094815621281e+05  4.3397276402934559e-02  9.1003068777074560e+01  2.3284853357887278e+02
 8.9593838601102590e+01 -6.1651213980920278e+06 -1.5224467818868242e+06  5.9318797774299292e+05  8.0501194978173249e-02  9.1012224414258782e+01  2.3284494524624364e+02
 9.3489221516190042e+01 -6.1651210122086853e+06 -1.5220922358071788e+06  5.9409499331504141e+05  1.1760510265438215e-01  9.1021377902529238e+01  2.3284135142573965e+02
 9.7384604316899996e+01 -6.1651204817909952e+06 -1.5217376540858690e+06  5.9500199485098489e+05  1.5470899361128776e-01  9.1030529243681869e+01  2.3283775211666702e+02
 1.0127998700322692e+02 -6.1651198068390293e+06 -1.5213830367312580e+06  5.9590898232945218e+05  1.9181287024411020e-01  9.1039678434977191e+01  2.3283414732010158e+02
 1.0517536957516208e+02 -6.1651189873528546e+06 -1.5210283837517232e+06  5.9681595572906942e+05  2.2891673089423217e-01  9.1048825478900270e+01  2.3283053703507858e+02
 1.0907075203270222e+02 -6.1651180233325455e+06 -1.5206736951556301e+06  5.9772291502846556e+05  2.6602057583063959e-01  9.1057970372990027e+01  2.3282692126256555e+02
 1.1296613437583903e+02 -6.1651169147781692e+06 -1.5203189709513541e+06  5.9862986020626849e+05  3.0312440146612507e-01  9.1067113118285704e+01  2.3282330000216598e+02
 1.1686151660456612e+02 -6.1651156616898160e+06 -1.5199642111472676e+06  5.9953679124110716e+05  3.4022820808137327e-01  9.1076253714154305e+01  2.3281967325413333e+02
 1.2075689871887812e+02 -6.1651142640675632e+06 -1.5196094157517429e+06  6.0044370811161119e+05  3.7733199811147067e-01  9.1085392161251164e+01  2.3281604101821310e+02
 1.2465228071876864e+02 -6.1651127219114928e+06 -1.5192545847731535e+06  6.0135061079640931e+05  4.1443576449811226e-01  9.1094528457667323e+01  2.3281240329516882e+02
 1.2854766260423233e+02 -6.1651110352217006e+06 -1.5188997182198770e+06  6.0225749927413149e+05  4.5153951206342330e-01  9.1103662604685866e+01  2.3280876008449641e+02
 1.3244304437526068e+02 -6.1651092039982732e+06 -1.5185448161002877e+06  6.0316437352340785e+05  4.8864323573556057e-01  9.1112794600482914e+01  2.3280511138692461e+02
 1.3633835468841394e+02 -6.1651072282788232e+06 -1.5181898849237328e+06  6.0407121691397089e+05  5.2574624394600045e-01  9.1121924227057065e+01  2.3280145728935085e+02


    END Ephemeris

    BEGIN MassProperties

        Mass		  1.0000000000000000e+03
        InertiaXX		  4.5000000000000000e+03
        InertiaYX		  0.0000000000000000e+00
        InertiaYY		  4.5000000000000000e+03
        InertiaZX		  0.0000000000000000e+00
        InertiaZY		  0.0000000000000000e+00
        InertiaZZ		  4.5000000000000000e+03

    END MassProperties

    BEGIN Attitude

        ScenarioEpoch		 18 Oct 2020 01:00:00.000000

        BEGIN Profile
            Name		 AircraftAtt
            UserNamed		 No
            StartTime		  0.0000000000000000e+00
            BEGIN ECFVelRadial
                Azimuth		  0.0000000000000000e+00
            END ECFVelRadial
        END Profile

    END Attitude

    BEGIN Swath

        SwathType		 ElevAngle
        ElevationAngle		  0.0000000000000000e+00
        HalfAngle		  0.0000000000000000e+00
        Distance		  0.0000000000000000e+00
        RepType		 NoSwath

    END Swath

    BEGIN Eclipse

        Sunlight		 Off
        SunlightLineStyle		 0
        SunlightLineWidth		 3
        SunlightMarkerStyle		 19

        Penumbra		 Off
        PenumbraLineStyle		 0
        PenumbraLineWidth		 3
        PenumbraMarkerStyle		 19

        Umbra		 Off
        UmbraLineStyle		 0
        UmbraLineWidth		 3
        UmbraMarkerStyle		 19

        SunlightPenumbraLine		 Off
        PenumbraUmbraLine		 Off

        SunlightColor		 #ffff00
        PenumbraColor		 #87cefa
        UmbraColor		 #0000ff
        UseCustomEclipseBodies		 No

    END Eclipse

    BEGIN RealTimeDef

        HistoryDuration		  1.8000000000000000e+03
        LookAheadDuration		  1.8000000000000000e+03

    END RealTimeDef


    BEGIN LineOfSightModel

        ModelType		 CbShape
        HeightAboveSurface		  0.0000000000000000e+00

    END LineOfSightModel


    BEGIN Extensions

        BEGIN LaserCAT
        END LaserCAT

        BEGIN ExternData
        END ExternData

        BEGIN RFI
        END RFI

        BEGIN ADFFileData
        END ADFFileData

        BEGIN AccessConstraints
            LineOfSight IncludeIntervals
        END AccessConstraints

        BEGIN ObjectCoverage
        END ObjectCoverage

        BEGIN Desc
        END Desc

        BEGIN Atmosphere
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Atmosphere_Extension">
    <SCOPE Class = "AtmosphereExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Atmosphere_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "InheritAtmosAbsorptionModel">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "AtmosAbsorptionModel">
            <VAR name = "Simple_Satcom">
                <SCOPE Class = "AtmosphericAbsorptionModel">
                    <VAR name = "Version">
                        <STRING>&quot;1.0.1 a&quot;</STRING>
                    </VAR>
                    <VAR name = "STKVersion">
                        <INT>1160</INT>
                    </VAR>
                    <VAR name = "ComponentName">
                        <STRING>&quot;Simple_Satcom&quot;</STRING>
                    </VAR>
                    <VAR name = "Description">
                        <STRING>&quot;Simple Satcom gaseous absorption model&quot;</STRING>
                    </VAR>
                    <VAR name = "Type">
                        <STRING>&quot;Simple Satcom&quot;</STRING>
                    </VAR>
                    <VAR name = "UserComment">
                        <STRING>&quot;Simple Satcom gaseous absorption model&quot;</STRING>
                    </VAR>
                    <VAR name = "ReadOnly">
                        <BOOL>false</BOOL>
                    </VAR>
                    <VAR name = "Clonable">
                        <BOOL>true</BOOL>
                    </VAR>
                    <VAR name = "Category">
                        <STRING>&quot;&quot;</STRING>
                    </VAR>
                    <VAR name = "SurfaceTemperature">
                        <QUANTITY Dimension = "Temperature" Unit = "K">
                            <REAL>293.15</REAL>
                        </QUANTITY>
                    </VAR>
                    <VAR name = "WaterVaporConcentration">
                        <QUANTITY Dimension = "Density" Unit = "g*m^-3">
                            <REAL>7.5</REAL>
                        </QUANTITY>
                    </VAR>
                </SCOPE>
            </VAR>
        </VAR>
        <VAR name = "EnableLocalRainData">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "LocalRainIsoHeight">
            <QUANTITY Dimension = "DistanceUnit" Unit = "m">
                <REAL>2000</REAL>
            </QUANTITY>
        </VAR>
        <VAR name = "LocalRainRate">
            <QUANTITY Dimension = "SlowRate" Unit = "mm*hr^-1">
                <REAL>1</REAL>
            </QUANTITY>
        </VAR>
        <VAR name = "LocalSurfaceTemp">
            <QUANTITY Dimension = "Temperature" Unit = "K">
                <REAL>293.15</REAL>
            </QUANTITY>
        </VAR>
    </SCOPE>
</VAR>        END Atmosphere

        BEGIN RadarCrossSection
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_RCS_Extension">
    <SCOPE Class = "RadarRCSExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_RCS_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Inherit">
            <BOOL>true</BOOL>
        </VAR>
    </SCOPE>
</VAR>        END RadarCrossSection

        BEGIN RadarClutter
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_Clutter_Extension">
    <SCOPE Class = "RadarClutterExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_Clutter_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Inherit">
            <BOOL>true</BOOL>
        </VAR>
    </SCOPE>
</VAR>        END RadarClutter

        BEGIN Ellipse
            TimesTrackVehStartTime		 Yes
        END Ellipse

        BEGIN Crdn
        END Crdn

        BEGIN Graphics

            BEGIN GenericGraphics
                IntvlHideShowAll		 Off
                ShowPassLabel		 Off
                ShowPathLabel		 Off
                TransformTrajectory		 On
                MinGfxGndTrkTimeStep		  0.0000000000000000e+00
                MaxGfxGndTrkTimeStep		  6.0000000000000000e+02
                MinGfxOrbitTimeStep		  0.0000000000000000e+00
                MaxGfxOrbitTimeStep		  6.0000000000000000e+02
                ShowGlintPoint		 Off
                ShowGlintColor		 #ffffff
                ShowGlintStyle		 2
            END GenericGraphics

            BEGIN AttributeData
                ShowGfx		 Off
                AttributeType		                Basic		
                BEGIN DefaultAttributes
                    Show		 Off
                    Inherit		 On
                    ShowLabel		 On
                    ShowGndTrack		 On
                    ShowGndMarker		 On
                    ShowOrbit		 On
                    ShowOrbitMarker		 On
                    ShowElsetNum		 Off
                    ShowSpecialSwath		 On
                    MarkerColor		 #00ff00
                    GroundTrackColor		 #00ff00
                    SwathColor		 #00ff00
                    LabelColor		 #00ff00
                    LineStyle		 0
                    LineWidth		 1
                    MarkerStyle		 19
                    FontStyle		 0
                    SwathLineStyle		 0
                    SpecSwathLineStyle		 1
                END DefaultAttributes

                BEGIN CustomIntervalList
                    BEGIN DefaultAttributes
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 19
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DefaultAttributes
                END CustomIntervalList

                BEGIN AccessIntervalsAttributes
                    BEGIN AttrDuringAccess
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END AttrDuringAccess
                    BEGIN AttrDuringNoAccess
                        Show		 Off
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ffff
                        GroundTrackColor		 #00ffff
                        SwathColor		 #00ffff
                        LabelColor		 #00ffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END AttrDuringNoAccess
                END AccessIntervalsAttributes

                BEGIN TimeComponentIntervalsAttributes
                    BEGIN DefaultAttributes
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DefaultAttributes
                END TimeComponentIntervalsAttributes

                BEGIN RealTimeAttributes
                    BEGIN HistoryAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ff00
                        GroundTrackColor		 #00ff00
                        SwathColor		 #00ff00
                        LabelColor		 #00ff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END HistoryAttr
                    BEGIN SplineAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ffff00
                        GroundTrackColor		 #ffff00
                        SwathColor		 #ffff00
                        LabelColor		 #ffff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END SplineAttr
                    BEGIN LookAheadAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ffffff
                        GroundTrackColor		 #ffffff
                        SwathColor		 #ffffff
                        LabelColor		 #ffffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END LookAheadAttr
                    BEGIN DropOutAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ff0000
                        GroundTrackColor		 #ff0000
                        SwathColor		 #ff0000
                        LabelColor		 #ff0000
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DropOutAttr
                END RealTimeAttributes
            END AttributeData

            BEGIN LeadTrailData
                GtLeadingType		 AllData
                GtTrailingType		 AllData
                OrbitLeadingType		 AllData
                OrbitTrailingType		 OnePass
            END LeadTrailData
            BEGIN SaaData
                ShowSaa		 Off
                ShowSaaFill		 Off
                SaaFillTranslucency		 0.7
                TrackSaa		 On
                SaaAltitude		 500000
            END SaaData
            BEGIN GroundTracks
                BEGIN GroundTrack
                    CentralBody		 Earth
                END GroundTrack
            END GroundTracks
            BEGIN WaypointData
                ShowWayptMarkers		 On
                ShowWayptTurnMarkers		 On
                ShowWayptMarkersExtEphem		 Off
                NewWayptMarkerShow		 On
                NewWayptShowLabel		 Off
                NewWayptMarkerStyle		 3
                WayptShow		 On
                WayptShowLabel		 Off
                WayptMarkerStyle		 3
            END WaypointData
            BEGIN EllipseSetGxData
                BEGIN DefEllipseSetGx
                    ShowStatic		 On
                    ShowDynamic		 On
                    UseLastDynPos		 Off
                    HoldLastDynPos		 Off
                    ShowName		 Off
                    ShowMarker		 On
                    MarkerStyle		 0
                    LineStyle		 0
                    LineWidth		 1
                END DefEllipseSetGx
            END EllipseSetGxData
        END Graphics

        BEGIN ContourGfx
            ShowContours		 Off
        END ContourGfx

        BEGIN Contours
            ActiveContourType		 Radar Cross Section

            BEGIN ContourSet Radar Cross Section
                Altitude		 0
                ShowAtAltitude		 Off
                Projected		 On
                Relative		 On
                ShowLabels		 Off
                LineWidth		 1
                DecimalDigits		 1
                ColorRamp		 On
                ColorRampStartColor		 #ff0000
                ColorRampEndColor		 #0000ff
                BEGIN ContourDefinition
                    BEGIN CntrAntAzEl
                        CoordinateSystem		 0
                        BEGIN AzElPatternDef
                            SetResolutionTogether		 0
                            NumAzPoints		 361
                            AzimuthRes		 1
                            MinAzimuth		 -180
                            MaxAzimuth		 180
                            NumElPoints		 91
                            ElevationRes		 1
                            MinElevation		 0
                            MaxElevation		 90
                        END AzElPatternDef
                    END CntrAntAzEl
                    BEGIN RCSContour
                        Frequency		 2997924580
                        ComputeType		 0
                    END RCSContour
                END ContourDefinition
            END ContourSet
        END Contours

        BEGIN VO
        END VO

        BEGIN 3dVolume
            ActiveVolumeType		 Radar Cross Section

            BEGIN VolumeSet Radar Cross Section
                Scale		 100
                MinimumDisplayedRcs		 1
                Frequency		  1.4500000000000000e+10
                ShowAsWireframe		 0
                BEGIN AzElPatternDef
                    SetResolutionTogether		 0
                    NumAzPoints		 50
                    AzimuthRes		 7.346938775510203
                    MinAzimuth		 -180
                    MaxAzimuth		 180
                    NumElPoints		 50
                    ElevationRes		 3.673469387755102
                    MinElevation		 0
                    MaxElevation		 180
                END AzElPatternDef
                ColorMethod		 1
                MinToMaxStartColor		 #ff0000
                MinToMaxStopColor		 #0000ff
                RelativeToMaximum		 0
            END VolumeSet
            BEGIN VolumeGraphics
                ShowContours		 No
                ShowVolume		 No
            END VolumeGraphics
        END 3dVolume

    END Extensions

    BEGIN SubObjects

    END SubObjects

END Ship

