stk.v.11.0
WrittenBy    STK_v11.6.0

BEGIN Radar

    Name		 Radar3
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_Object">
    <SCOPE Class = "CommRadarObject">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_Object&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar Object&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar Object&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar Object&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Model">
            <VAR name = "Bistatic_Receiver">
                <SCOPE Class = "Radar System">
                    <VAR name = "Version">
                        <STRING>&quot;1.0.0 a&quot;</STRING>
                    </VAR>
                    <VAR name = "STKVersion">
                        <INT>1160</INT>
                    </VAR>
                    <VAR name = "ComponentName">
                        <STRING>&quot;Bistatic_Receiver&quot;</STRING>
                    </VAR>
                    <VAR name = "Description">
                        <STRING>&quot;Bistatic Receiver&quot;</STRING>
                    </VAR>
                    <VAR name = "Type">
                        <STRING>&quot;Bistatic Receiver&quot;</STRING>
                    </VAR>
                    <VAR name = "UserComment">
                        <STRING>&quot;Bistatic Receiver&quot;</STRING>
                    </VAR>
                    <VAR name = "ReadOnly">
                        <BOOL>false</BOOL>
                    </VAR>
                    <VAR name = "Clonable">
                        <BOOL>true</BOOL>
                    </VAR>
                    <VAR name = "Category">
                        <STRING>&quot;@Top&quot;</STRING>
                    </VAR>
                    <VAR name = "AntennaControl">
                        <SCOPE>
                            <VAR name = "AntennaReferenceType">
                                <STRING>&quot;Embed&quot;</STRING>
                            </VAR>
                            <VAR name = "Antenna">
                                <VAR name = "Phased_Array">
                                    <SCOPE Class = "Antenna">
                                        <VAR name = "Version">
                                            <STRING>&quot;1.0.0 a&quot;</STRING>
                                        </VAR>
                                        <VAR name = "STKVersion">
                                            <INT>1160</INT>
                                        </VAR>
                                        <VAR name = "ComponentName">
                                            <STRING>&quot;Phased_Array&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Description">
                                            <STRING>&quot;Models a phased array antenna&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Phased Array&quot;</STRING>
                                        </VAR>
                                        <VAR name = "UserComment">
                                            <STRING>&quot;Models a phased array antenna&quot;</STRING>
                                        </VAR>
                                        <VAR name = "ReadOnly">
                                            <BOOL>false</BOOL>
                                        </VAR>
                                        <VAR name = "Clonable">
                                            <BOOL>true</BOOL>
                                        </VAR>
                                        <VAR name = "Category">
                                            <STRING>&quot;@Top&quot;</STRING>
                                        </VAR>
                                        <VAR name = "DesignFrequency">
                                            <QUANTITY Dimension = "FrequencyUnit" Unit = "Hz">
                                                <REAL>1575000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "BeamDirectionProvider">
                                            <VAR name = "Object">
                                                <SCOPE Class = "Direction Provider">
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                    <VAR name = "Directions">
                                                        <LISTLINKTOOBJ />
                                                    </VAR>
                                                    <VAR name = "AzimuthSteeringLimitA">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>-1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "AzimuthSteeringLimitB">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "ElevationSteeringLimitA">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>-1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "ElevationSteeringLimitB">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "LimitsExceededBehaviorType">
                                                        <STRING>&quot;Clamp-To-Limit&quot;</STRING>
                                                    </VAR>
                                                    <VAR name = "UseDefaultDirection">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                    <VAR name = "Type">
                                                        <STRING>&quot;Object&quot;</STRING>
                                                    </VAR>
                                                    <VAR name = "ComponentName">
                                                        <STRING>&quot;Object&quot;</STRING>
                                                    </VAR>
                                                </SCOPE>
                                            </VAR>
                                        </VAR>
                                        <VAR name = "NullDirectionProvider">
                                            <VAR name = "Object">
                                                <SCOPE Class = "Direction Provider">
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                    <VAR name = "Directions">
                                                        <LISTLINKTOOBJ />
                                                    </VAR>
                                                    <VAR name = "AzimuthSteeringLimitA">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>-1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "AzimuthSteeringLimitB">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "ElevationSteeringLimitA">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>-1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "ElevationSteeringLimitB">
                                                        <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                            <REAL>1.570796326794897</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "LimitsExceededBehaviorType">
                                                        <STRING>&quot;Clamp-To-Limit&quot;</STRING>
                                                    </VAR>
                                                    <VAR name = "UseDefaultDirection">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                    <VAR name = "Type">
                                                        <STRING>&quot;Object&quot;</STRING>
                                                    </VAR>
                                                    <VAR name = "ComponentName">
                                                        <STRING>&quot;Object&quot;</STRING>
                                                    </VAR>
                                                </SCOPE>
                                            </VAR>
                                        </VAR>
                                        <VAR name = "BacklobeSuppressionType">
                                            <STRING>&quot;Constant&quot;</STRING>
                                        </VAR>
                                        <VAR name = "BacklobeSuppression">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>100</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "IncludeElementFactor">
                                            <BOOL>true</BOOL>
                                        </VAR>
                                        <VAR name = "ElementFactorExponent">
                                            <REAL>1</REAL>
                                        </VAR>
                                        <VAR name = "Elements">
                                            <LIST>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>0</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>1</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>2</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>3</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>4</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>5</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>6</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>7</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>8</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>9</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>10</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>11</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>12</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>13</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>14</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>15</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>16</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>17</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>18</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>19</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>20</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>21</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>22</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>23</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>24</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>25</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>26</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>-0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>27</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>28</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>29</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>30</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>31</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>32</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>33</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>34</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>35</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>36</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>37</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.25</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>38</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>39</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>40</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>41</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>42</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>43</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>44</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>45</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>46</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.4330127018922193</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>47</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.299038105676658</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>48</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>0.75</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>2.165063509461096</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>49</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>50</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>-0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>51</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>52</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>0.8660254037844386</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>53</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                                <SCOPE>
                                                    <VAR name = "X">
                                                        <REAL>1</REAL>
                                                    </VAR>
                                                    <VAR name = "Y">
                                                        <REAL>1.732050807568877</REAL>
                                                    </VAR>
                                                    <VAR name = "Id">
                                                        <INT>54</INT>
                                                    </VAR>
                                                    <VAR name = "Enabled">
                                                        <BOOL>false</BOOL>
                                                    </VAR>
                                                </SCOPE>
                                            </LIST>
                                        </VAR>
                                        <VAR name = "ElementConfigDesigner">
                                            <VAR name = "Hexagon">
                                                <SCOPE Class = "ElementConfigDesigner">
                                                    <VAR name = "Type">
                                                        <STRING>&quot;Hexagon&quot;</STRING>
                                                    </VAR>
                                                    <VAR name = "Frequency">
                                                        <QUANTITY Dimension = "FrequencyUnit" Unit = "Hz">
                                                            <REAL>1575000000</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "Lattice">
                                                        <STRING>&quot;Triangular&quot;</STRING>
                                                    </VAR>
                                                    <VAR name = "Equilateral">
                                                        <BOOL>true</BOOL>
                                                    </VAR>
                                                    <VAR name = "NumElementsX">
                                                        <INT>3</INT>
                                                    </VAR>
                                                    <VAR name = "NumElementsY">
                                                        <INT>11</INT>
                                                    </VAR>
                                                    <VAR name = "SpacingUnit">
                                                        <STRING>&quot;Wavelength Ratio&quot;</STRING>
                                                    </VAR>
                                                    <VAR name = "SpacingX">
                                                        <REAL>0.5</REAL>
                                                    </VAR>
                                                    <VAR name = "SpacingY">
                                                        <REAL>0.4330127018922193</REAL>
                                                    </VAR>
                                                </SCOPE>
                                            </VAR>
                                        </VAR>
                                        <VAR name = "Beamformer">
                                            <VAR name = "Mvdr">
                                                <SCOPE Class = "BeamFormer">
                                                    <VAR name = "Constraint">
                                                        <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                            <REAL>1.99526231496888</REAL>
                                                        </QUANTITY>
                                                    </VAR>
                                                    <VAR name = "Type">
                                                        <STRING>&quot;Mvdr&quot;</STRING>
                                                    </VAR>
                                                </SCOPE>
                                            </VAR>
                                        </VAR>
                                        <VAR name = "ShowGrid">
                                            <BOOL>true</BOOL>
                                        </VAR>
                                        <VAR name = "ShowLabels">
                                            <BOOL>true</BOOL>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "Orientation">
                                <VAR name = "Azimuth Elevation">
                                    <SCOPE Class = "Orientation">
                                        <VAR name = "AzimuthAngle">
                                            <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "ElevationAngle">
                                            <QUANTITY Dimension = "AngleUnit" Unit = "rad">
                                                <REAL>1.570796326794897</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "AboutBoresight">
                                            <STRING>&quot;Rotate&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Azimuth Elevation&quot;</STRING>
                                        </VAR>
                                        <VAR name = "XPositionOffset">
                                            <QUANTITY Dimension = "SmallDistanceUnit" Unit = "m">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "YPositionOffset">
                                            <QUANTITY Dimension = "SmallDistanceUnit" Unit = "m">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "ZPositionOffset">
                                            <QUANTITY Dimension = "SmallDistanceUnit" Unit = "m">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                        </SCOPE>
                    </VAR>
                    <VAR name = "Jamming">
                        <SCOPE>
                            <VAR name = "Enabled">
                                <BOOL>false</BOOL>
                            </VAR>
                            <VAR name = "Jammers">
                                <SCOPE>
                                    <VAR name = "ObjectList">
                                        <LISTLINKTOOBJ />
                                    </VAR>
                                </SCOPE>
                            </VAR>
                        </SCOPE>
                    </VAR>
                    <VAR name = "Receiver">
                        <SCOPE>
                            <VAR name = "Frequency">
                                <QUANTITY Dimension = "FrequencyUnit" Unit = "Hz">
                                    <REAL>1575000000</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "ComputeSystemNoiseTemp">
                                <STRING>&quot;Constant&quot;</STRING>
                            </VAR>
                            <VAR name = "ConstantSystemNoiseTemp">
                                <QUANTITY Dimension = "Temperature" Unit = "K">
                                    <REAL>290</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "ANT2LNALineLoss">
                                <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                    <REAL>1</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "ANT2LNALineTemperature">
                                <QUANTITY Dimension = "Temperature" Unit = "K">
                                    <REAL>290</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "LNA2RcvrLineLoss">
                                <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                    <REAL>1</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "LNA2RcvrLineTemperature">
                                <QUANTITY Dimension = "Temperature" Unit = "K">
                                    <REAL>290</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "LNAGain">
                                <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                    <REAL>1</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "LNANoiseFigure">
                                <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                    <REAL>1.258925411794167</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "LNATemperature">
                                <QUANTITY Dimension = "Temperature" Unit = "K">
                                    <REAL>290</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "AntennaNoise">
                                <SCOPE>
                                    <VAR name = "ComputeOption">
                                        <STRING>&quot;Constant&quot;</STRING>
                                    </VAR>
                                    <VAR name = "ComputeRainNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeAtmosAbsorpNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeUrbanTerrestrialNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeCloudsFogNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeTropoScintNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeIonoFadingNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeSunNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeEarthNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeCosmicNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "ComputeExternalNoise">
                                        <BOOL>false</BOOL>
                                    </VAR>
                                    <VAR name = "InheritScenarioEarthTemperature">
                                        <BOOL>true</BOOL>
                                    </VAR>
                                    <VAR name = "LocalEarthTemperature">
                                        <QUANTITY Dimension = "Temperature" Unit = "K">
                                            <REAL>290</REAL>
                                        </QUANTITY>
                                    </VAR>
                                    <VAR name = "ConstantNoiseTemp">
                                        <QUANTITY Dimension = "Temperature" Unit = "K">
                                            <REAL>290</REAL>
                                        </QUANTITY>
                                    </VAR>
                                    <VAR name = "OtherNoiseTemp">
                                        <QUANTITY Dimension = "Temperature" Unit = "K">
                                            <REAL>0</REAL>
                                        </QUANTITY>
                                    </VAR>
                                </SCOPE>
                            </VAR>
                            <VAR name = "LNABandwidth">
                                <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                    <REAL>30000000</REAL>
                                </QUANTITY>
                            </VAR>
                            <VAR name = "RainOutagePercent">
                                <PROP name = "FormatString">
                                    <STRING>&quot;%#6.3f&quot;</STRING>
                                </PROP>
                                <REAL>0.1</REAL>
                            </VAR>
                            <VAR name = "UseRain">
                                <BOOL>true</BOOL>
                            </VAR>
                            <VAR name = "AdditionalGainsLosses">
                                <SCOPE>
                                    <VAR name = "GainLossList">
                                        <LIST />
                                    </VAR>
                                </SCOPE>
                            </VAR>
                            <VAR name = "EnableRfSTC">
                                <BOOL>false</BOOL>
                            </VAR>
                            <VAR name = "EnableIfSTC">
                                <BOOL>false</BOOL>
                            </VAR>
                            <VAR name = "RfSTC">
                                <VAR name = "Decay Factor">
                                    <SCOPE Class = "STC">
                                        <VAR name = "MaxValue">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StartValue">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>10</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StepSize">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1.258925411794167</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StartRange">
                                            <QUANTITY Dimension = "DistanceUnit" Unit = "m">
                                                <REAL>926</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StopRange">
                                            <QUANTITY Dimension = "DistanceUnit" Unit = "m">
                                                <REAL>92600</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "DecayFactor">
                                            <REAL>2</REAL>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Decay Factor&quot;</STRING>
                                        </VAR>
                                        <VAR name = "ComponentName">
                                            <STRING>&quot;Decay Factor&quot;</STRING>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "IfSTC">
                                <VAR name = "Decay Factor">
                                    <SCOPE Class = "STC">
                                        <VAR name = "MaxValue">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StartValue">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>10</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StepSize">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1.258925411794167</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StartRange">
                                            <QUANTITY Dimension = "DistanceUnit" Unit = "m">
                                                <REAL>926</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "StopRange">
                                            <QUANTITY Dimension = "DistanceUnit" Unit = "m">
                                                <REAL>92600</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "DecayFactor">
                                            <REAL>2</REAL>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Decay Factor&quot;</STRING>
                                        </VAR>
                                        <VAR name = "ComponentName">
                                            <STRING>&quot;Decay Factor&quot;</STRING>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "UseFilter">
                                <BOOL>false</BOOL>
                            </VAR>
                            <VAR name = "Filter">
                                <VAR name = "Butterworth">
                                    <SCOPE Class = "Filter">
                                        <VAR name = "Version">
                                            <STRING>&quot;1.0.0 a&quot;</STRING>
                                        </VAR>
                                        <VAR name = "STKVersion">
                                            <INT>1160</INT>
                                        </VAR>
                                        <VAR name = "ComponentName">
                                            <STRING>&quot;Butterworth&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Description">
                                            <STRING>&quot;General form of nth order Butterworth filter with flat passband and stopband regions&quot;</STRING>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Butterworth&quot;</STRING>
                                        </VAR>
                                        <VAR name = "UserComment">
                                            <STRING>&quot;General form of nth order Butterworth filter with flat passband and stopband regions&quot;</STRING>
                                        </VAR>
                                        <VAR name = "ReadOnly">
                                            <BOOL>false</BOOL>
                                        </VAR>
                                        <VAR name = "Clonable">
                                            <BOOL>true</BOOL>
                                        </VAR>
                                        <VAR name = "Category">
                                            <STRING>&quot;&quot;</STRING>
                                        </VAR>
                                        <VAR name = "LowerBandwidthLimit">
                                            <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                                <REAL>-20000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "UpperBandwidthLimit">
                                            <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                                <REAL>20000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "InsertionLoss">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "Order">
                                            <INT>4</INT>
                                        </VAR>
                                        <VAR name = "CutoffFrequency">
                                            <QUANTITY Dimension = "BandwidthUnit" Unit = "Hz">
                                                <REAL>10000000</REAL>
                                            </QUANTITY>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "UsePolarization">
                                <BOOL>true</BOOL>
                            </VAR>
                            <VAR name = "Polarization">
                                <VAR name = "Left-hand Circular">
                                    <SCOPE Class = "Polarization">
                                        <VAR name = "CrossPolLeakage">
                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                <REAL>1e-06</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "Type">
                                            <STRING>&quot;Left-hand Circular&quot;</STRING>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                            </VAR>
                            <VAR name = "EnableOrthoPolarization">
                                <BOOL>false</BOOL>
                            </VAR>
                        </SCOPE>
                    </VAR>
                    <VAR name = "Mode">
                        <VAR name = "Search_Track">
                            <SCOPE Class = "Bistatic Receiver Radar Mode">
                                <VAR name = "Version">
                                    <STRING>&quot;1.0.0 a&quot;</STRING>
                                </VAR>
                                <VAR name = "STKVersion">
                                    <INT>1160</INT>
                                </VAR>
                                <VAR name = "ComponentName">
                                    <STRING>&quot;Search_Track&quot;</STRING>
                                </VAR>
                                <VAR name = "Description">
                                    <STRING>&quot;Search Track&quot;</STRING>
                                </VAR>
                                <VAR name = "Type">
                                    <STRING>&quot;Search Track&quot;</STRING>
                                </VAR>
                                <VAR name = "UserComment">
                                    <STRING>&quot;Search Track&quot;</STRING>
                                </VAR>
                                <VAR name = "ReadOnly">
                                    <BOOL>false</BOOL>
                                </VAR>
                                <VAR name = "Clonable">
                                    <BOOL>true</BOOL>
                                </VAR>
                                <VAR name = "Category">
                                    <STRING>&quot;&quot;</STRING>
                                </VAR>
                                <VAR name = "ClutterFilters">
                                    <SCOPE>
                                        <VAR name = "EnableMainLobeClutter">
                                            <BOOL>false</BOOL>
                                        </VAR>
                                        <VAR name = "MainLobeClutterBandwidth">
                                            <QUANTITY Dimension = "DopplerVelocityUnit" Unit = "m/s">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                        <VAR name = "EnableSideLobeClutter">
                                            <BOOL>false</BOOL>
                                        </VAR>
                                        <VAR name = "SideLobeClutterBandwidth">
                                            <QUANTITY Dimension = "DopplerVelocityUnit" Unit = "m/s">
                                                <REAL>0</REAL>
                                            </QUANTITY>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                                <VAR name = "Waveform">
                                    <VAR name = "Continuous Wave">
                                        <SCOPE Class = "SearchTrackBistaticReceiverWaveform">
                                            <VAR name = "PulseIntegration">
                                                <VAR name = "Goal SNR">
                                                    <SCOPE Class = "ContinuousWavePulseIntegrationMethod">
                                                        <VAR name = "SNR">
                                                            <QUANTITY Dimension = "RatioUnit" Unit = "units">
                                                                <REAL>39.81071705534973</REAL>
                                                            </QUANTITY>
                                                        </VAR>
                                                        <VAR name = "Type">
                                                            <STRING>&quot;Goal SNR&quot;</STRING>
                                                        </VAR>
                                                    </SCOPE>
                                                </VAR>
                                            </VAR>
                                            <VAR name = "ProbabilityOfDetection">
                                                <SCOPE>
                                                    <VAR name = "ProbabilityFalseAlarm">
                                                        <REAL>1e-06</REAL>
                                                    </VAR>
                                                </SCOPE>
                                            </VAR>
                                            <VAR name = "Type">
                                                <STRING>&quot;Continuous Wave&quot;</STRING>
                                            </VAR>
                                        </SCOPE>
                                    </VAR>
                                </VAR>
                            </SCOPE>
                        </VAR>
                    </VAR>
                    <VAR name = "BistaticTransmitterSelectionStrategy">
                        <VAR name = "Static Transmitter">
                            <SCOPE Class = "Bistatic Transmitter Selection Strategy">
                                <VAR name = "BistaticTransmitters">
                                    <SCOPE>
                                        <VAR name = "ObjectList">
                                            <LISTLINKTOOBJ>
                                                <STRING>&quot;Satellite/gps-07_svn48/Radar/Radar1&quot;</STRING>
                                            </LISTLINKTOOBJ>
                                        </VAR>
                                    </SCOPE>
                                </VAR>
                                <VAR name = "Type">
                                    <STRING>&quot;Static Transmitter&quot;</STRING>
                                </VAR>
                                <VAR name = "ComponentName">
                                    <STRING>&quot;Static Transmitter&quot;</STRING>
                                </VAR>
                            </SCOPE>
                        </VAR>
                    </VAR>
                    <VAR name = "ClutterEnabled">
                        <BOOL>false</BOOL>
                    </VAR>
                </SCOPE>
            </VAR>
        </VAR>
    </SCOPE>
</VAR>
END Radar

BEGIN Extensions

    BEGIN ExternData
    END ExternData

    BEGIN ADFFileData
    END ADFFileData

    BEGIN AccessConstraints
        LineOfSight IncludeIntervals
        RdrXmtTgtAccess IncludeIntervals
    END AccessConstraints

    BEGIN ObjectCoverage
    END ObjectCoverage

    BEGIN Desc
    END Desc

    BEGIN Refraction
        RefractionModel		 Effective Radius Method

        UseRefractionInAccess		 No

        BEGIN ModelData
            RefractionCeiling		  5.0000000000000000e+03
            MaxTargetAltitude		  1.0000000000000000e+04
            EffectiveRadius		  1.3333333333333299e+00

            UseExtrapolation		 Yes


        END ModelData
    END Refraction

    BEGIN Crdn
    END Crdn

    BEGIN Graphics

        BEGIN Graphics

            ShowRdr		 Yes
            ShowXmtTgt		 No
            ShowXmtRdr		 No
            ShowContour		 No
            UseSinglePulse		 Yes
            LinearSNR		 39.81072
            LineWidth		 1
            LineColor		 #9b30ff
            LineStyle		 1
            XmtTgtWidth		 1
            XmtTgtColor		 #9b30ff
            XmtTgtStyle		 2
            XmtRdrWidth		 1
            XmtRdrColor		 #9b30ff
            XmtRdrStyle		 2
            ShowRdrTgtGrp		 No
            RdrTgtGrpMarker		 0
            RdrTgtGrpColor		 #00ff00
            ShowXmtTgtGrp		 No
            XmtTgtGrpMarker		 0
            XmtTgtGrpColor		 #00ff00
            ShowXmtRdrGrp		 No
            XmtRdrGrpMarker		 0
            XmtRdrGrpColor		 #00ff00
            ShowClutter		 No
            ClutterColor		 #0000ff

            BEGIN Antenna


                BEGIN Graphics

                    ShowGfx		 On
                    Relative		 On
                    ShowBoresight		 On
                    BoresightMarker		 4
                    BoresightColor		 #ffffff

                END Graphics

            END Antenna


        END Graphics
    END Graphics

    BEGIN ContourGfx
        ShowContours		 Off
    END ContourGfx

    BEGIN Contours
        ActiveContourType		 Antenna Gain

        BEGIN ContourSet Antenna Gain
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntAzEl
                    CoordinateSystem		 0
                    BEGIN AzElPatternDef
                        SetResolutionTogether		 0
                        NumAzPoints		 181
                        AzimuthRes		 2
                        MinAzimuth		 -180
                        MaxAzimuth		 180
                        NumElPoints		 91
                        ElevationRes		 1
                        MinElevation		 0
                        MaxElevation		 90
                    END AzElPatternDef
                END CntrAntAzEl
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet EIRP
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntAzEl
                    CoordinateSystem		 0
                    BEGIN AzElPatternDef
                        SetResolutionTogether		 0
                        NumAzPoints		 181
                        AzimuthRes		 2
                        MinAzimuth		 -180
                        MaxAzimuth		 180
                        NumElPoints		 91
                        ElevationRes		 1
                        MinElevation		 0
                        MaxElevation		 90
                    END AzElPatternDef
                END CntrAntAzEl
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet Flux Density
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntLatLon
                    SetResolutionTogether		 true
                    Resolution		 1 1
                    ElevationAngleConstraint		 90
                    BEGIN LatLonSphGrid
                        Centroid		 0 0
                        ConeAngle		 0
                        NumPts		 181 91
                        Altitude		 0
                    END LatLonSphGrid
                END CntrAntLatLon
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet RIP
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntLatLon
                    SetResolutionTogether		 true
                    Resolution		 1 1
                    ElevationAngleConstraint		 90
                    BEGIN LatLonSphGrid
                        Centroid		 0 0
                        ConeAngle		 0
                        NumPts		 181 91
                        Altitude		 0
                    END LatLonSphGrid
                END CntrAntLatLon
            END ContourDefinition
        END ContourSet

        BEGIN ContourSet Spectral Flux Density
            Altitude		 0
            ShowAtAltitude		 Off
            Projected		 On
            Relative		 On
            ShowLabels		 Off
            LineWidth		 1
            DecimalDigits		 1
            ColorRamp		 On
            ColorRampStartColor		 #0000ff
            ColorRampEndColor		 #ff0000
            BEGIN ContourDefinition
                BEGIN CntrAntLatLon
                    SetResolutionTogether		 true
                    Resolution		 1 1
                    ElevationAngleConstraint		 90
                    BEGIN LatLonSphGrid
                        Centroid		 0 0
                        ConeAngle		 0
                        NumPts		 181 91
                        Altitude		 0
                    END LatLonSphGrid
                END CntrAntLatLon
            END ContourDefinition
        END ContourSet
    END Contours

    BEGIN VO
    END VO

    BEGIN 3dVolume
        ActiveVolumeType		 Antenna Beam

        BEGIN VolumeSet Antenna Beam
            Scale		 4
            MinimumDisplayedGain		 1
            Frequency		 2997924580
            ShowAsWireframe		 0
            CoordinateSystem		 0
            BEGIN AzElPatternDef
                SetResolutionTogether		 0
                NumAzPoints		 181
                AzimuthRes		 2
                MinAzimuth		 -180
                MaxAzimuth		 180
                NumElPoints		 91
                ElevationRes		 1
                MinElevation		 0
                MaxElevation		 90
            END AzElPatternDef
            ColorMethod		 1
            MinToMaxStartColor		 #ff0000
            MinToMaxStopColor		 #0000ff
            RelativeToMaximum		 0
        END VolumeSet
        BEGIN VolumeGraphics
            ShowContours		 No
            ShowVolume		 No
        END VolumeGraphics
    END 3dVolume

END Extensions
